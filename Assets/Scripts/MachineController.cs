﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineController : MonoBehaviour
{
    private Rigidbody2D machine;
    private Collider2D collision;

    // Start is called before the first frame update
    void Start()
    {
        machine= GetComponent<Rigidbody2D>();
        collision = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
            if (collision.tag == "machine_lefth")
            {
                machine.transform.Translate(15f * Time.deltaTime, 0, 0);
            }
            
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (collision.tag == "machine_lefth")
            {
                machine.transform.Translate(-15f * Time.deltaTime, 0, 0);
            }
        }
    }
}
