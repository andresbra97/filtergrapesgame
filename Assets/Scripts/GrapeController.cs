﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapeController : MonoBehaviour
{
    private Rigidbody2D grape;

    // Start is called before the first frame update
    void Start()
    {
        grape = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            grape.gravityScale = 1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "machine_lefth")
        {
            Destroy(gameObject);

        } else if (collision.tag == "container")
        {
            Destroy(gameObject);

            if (grape.tag == "grape")
            {
                Percentage.percentage.RaisePercentage(1);
            }
            else if(grape.tag == "grape_green")
            {
                Percentage.percentage.RaisePercentage(-1);
            } else if (grape.tag == "rama")
            {
                Percentage.percentage.RaisePercentage(-2);
            } else if (grape.tag == "racimo")
            {
                Percentage.percentage.RaisePercentage(3);
            }
        }

    }
}
