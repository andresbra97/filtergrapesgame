﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Percentage : MonoBehaviour
{
    public static Percentage percentage;

    public Text percentageText;
    int percentageInt = 0;

    void Start()
    {
        percentage = this;
    }

    public void RaisePercentage(int p)
    {
        if (p<0) {
            percentageInt += p;

            if (percentageInt < 0) {
                percentageInt = 0;
            }

            percentageText.text = percentageInt + " %";
        }
        else
        {
            percentageInt += p;
            percentageText.text = percentageInt + " %";
        }
    }

}
