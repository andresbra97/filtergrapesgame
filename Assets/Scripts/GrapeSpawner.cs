﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrapeSpawner : MonoBehaviour
{
    public GameObject grape;
    public GameObject parent;

    public Sprite grapeGreen;
    public Sprite rama;
    public Sprite racimo;



    private void Start()
    {
        grapeGreen = Resources.Load<Sprite>("Sprites/GrapeGreen");
        rama = Resources.Load<Sprite>("Sprites/rama");
        racimo = Resources.Load<Sprite>("Sprites/racimo");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Vector3 position = new Vector3(grape.transform.localPosition.x, grape.transform.localPosition.y, 0);
            Vector3 scale = new Vector3(grape.transform.localScale.x, grape.transform.localScale.y, grape.transform.localScale.z);
            
            GameObject grapePrefab = Instantiate(grape);
            grapePrefab.transform.SetParent(parent.transform);

            grapePrefab.transform.localPosition = position;
            grapePrefab.transform.localScale = scale;

            Color(grapePrefab);
        }
    }

    public void Color(GameObject grapePrefab)
    {

        int r = Random.Range(1, 5);

        if (r == 2)
        {
            grapePrefab.transform.GetChild(0).GetComponent<Image>().sprite = grapeGreen;
            grapePrefab.tag = "grape_green";
        } else if (r == 4)
        {
            grapePrefab.transform.GetChild(0).GetComponent<Image>().sprite = rama;
            grapePrefab.tag = "rama";
        } else if (r==3)
        {
            grapePrefab.transform.GetChild(0).GetComponent<Image>().sprite = racimo;
            grapePrefab.tag = "racimo";
        }
    }
}
